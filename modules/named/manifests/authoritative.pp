class named::authoritative inherits named {
	file { '/etc/bind/named.conf.shared-keys':
		mode    => '0640',
		owner   => root,
		group   => bind,
	}
}
