class roles::security_mirror {
	include roles::archvsync_base

	# security abusers
	#  198.108.67.48 DoS against our rsync service
	@ferm::rule { 'dsa-security-abusers':
		prio  => "005",
		rule  => "saddr ( 198.108.67.48/32 ) DROP",
	}

	$binds = $::hostname ? {
		mirror-anu      => [ '150.203.164.61', '[2001:388:1034:2900::3d]' ],
		mirror-isc      => [ '149.20.4.14', '[2001:4f8:1:c::14]' ],
		mirror-umn      => [ '128.101.240.215', '[2607:ea00:101:3c0b::1deb:215]' ],
		schmelzer       => [ '217.196.149.233', '[2a02:16a8:dc41:100::233]' ],
		default         => [ '[::]' ],
	}

	include apache2::expires
	include apache2::rewrite

	apache2::site { '010-security.debian.org':
		site   => 'security.debian.org',
		content => template('roles/security_mirror/security.debian.org.erb')
	}

	$mirrors = hiera('roles.security_mirror', {})
	$fastly_mirrors = $mirrors.filter |$h| { $h[1]['fastly-backend'] }
	$hosts_to_check = $fastly_mirrors.map |$h| { $h[1]['service-hostname'] }

	roles::mirror_health { 'security':
		check_hosts   => $hosts_to_check,
		check_service => 'security',
		url           => 'http://security.backend.mirrors.debian.org/debian-security/dists/stable/updates/Release',
		health_url    => 'http://security.backend.mirrors.debian.org/_health',
        }

	rsync::site { 'security':
		source      => 'puppet:///modules/roles/security_mirror/rsyncd.conf',
		max_clients => 100,
		binds       => $binds,
	}

	$onion_v4_addr = hiera("roles.security_mirror", {})
		.dig($::fqdn, 'onion_v4_address')
	if $onion_v4_addr {
		onion::service { 'security.debian.org':
			port => 80,
			target_port => 80,
			target_address => $onion_v4_addr,
		}
	}
}
