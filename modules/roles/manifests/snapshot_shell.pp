class roles::snapshot_shell {
	# give ftpmaster/the dak user some environment so they can poll the
	# list of files we have
	file { '/etc/ssh/userkeys/dak':
		ensure => present,
		owner => dak,
		mode => '0644',
	}
	file { '/home/dak':
		ensure => link,
		target => '/srv/ftp-master.debian.org/home',
	}
	file { '/srv/ftp-master.debian.org':
		ensure => directory,
		owner => 'root',
		group => 'debadmin',
		mode => '02775',
	}
	file { '/srv/ftp-master.debian.org/home':
		ensure => directory,
		owner => 'dak',
		group => 'debadmin',
		mode => '02755',
	}
}
