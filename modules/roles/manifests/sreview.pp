class roles::sreview {
	ssl::service { 'sreview.debian.net': notify  => Exec['service apache2 reload'], key => true, }

	$now = Timestamp()
	$date = $now.strftime('%F')

	if versioncmp($date, '2019-08-15') <= 0 {
		@ferm::rule { 'temporary-dc19-access':
			description     => 'temporarily allow DC19 access, cf. RT#7845',
			rule            => '&SERVICE_RANGE(tcp, 5432, ( 200.134.17.48/28 ))',
		}
	} else {
		# also clean up pg_hba on vittoria
		notify {"Temporary DC19 ferm rule expired, cf. RT#7845":
			loglevel => warning, }
	}
}
