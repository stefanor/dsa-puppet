class roles::security_tracker {
	include apache2::ssl
	include apache2::proxy_http
	include apache2::expires

	apache2::module { 'cache_disk':
		ensure => absent,
	}

	# security-tracker abusers
	#  66.170.99.1  20180706 excessive number of requests
	#  66.170.99.2  20180706 excessive number of requests
	@ferm::rule { 'dsa-sectracker-abusers':
		prio  => "005",
		rule  => "saddr (66.170.99.1 66.170.99.2) DROP",
	}


	ssl::service { 'security-tracker.debian.org':
		notify  => Exec['service apache2 reload'],
		key => true,
	}

	apache2::site { 'security-tracker.debian.org':
		site   => 'security-tracker.debian.org',
		content => template('roles/apache-security-tracker.debian.org.conf.erb')
	}

	# traffic shaping http traffic
	#@ferm::rule { 'dsa-security-tracker-shape':
	#	table => 'mangle',
	#	chain => 'OUTPUT',
	#	rule  => "proto tcp sport 443 MARK set-mark 20",
	#}

	file { '/usr/local/sbin/traffic-shape':
		mode   => '0755',
		content => template('roles/security-tracker/traffic-shape'),
		notify => Exec['/usr/local/sbin/traffic-shape'],
	}
	exec { '/usr/local/sbin/traffic-shape':
		refreshonly => true
	}
}
