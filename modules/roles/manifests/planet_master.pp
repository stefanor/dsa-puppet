class roles::planet_master {
	include apache2::ssl
	apache2::config { 'puppet-debianhosts':
		ensure => 'absent',
	}
	apache2::site { 'planet-master.debian.org':
		content => template('roles/planet_master/planet-master.debian.org.erb')
	}
	ssl::service { 'planet-master.debian.org':
		notify => Exec['service apache2 reload'],
		key => true,
	}
}
