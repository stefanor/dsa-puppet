# do not modify - this file is maintained via puppet

AddType application/font-woff2 .woff2

Use common-debian-service-https-redirect * wafertest.debconf.org

WSGIDaemonProcess wafertest \
  processes=3 threads=2 \
  user=www-data group=debconf-web maximum-requests=750 umask=0007 display-name=wsgi-wafertest.debconf.org \
  python-path=/srv/debconf-web/wafertest.debconf.org/dc19/:/srv/debconf-web/wafertest.debconf.org/dc19/ve/lib/python3.5/site-packages/

<VirtualHost *:443>
  ServerAdmin admin@debconf.org
  ServerName wafertest.debconf.org

  ErrorLog  /var/log/apache2/wafertest.debconf.org-error.log
  CustomLog /var/log/apache2/wafertest.debconf.org-access.log combined

  Use common-debian-service-ssl wafertest.debconf.org
  Use common-ssl-HSTS

  Header always set Referrer-Policy "same-origin"
  Header always set X-Content-Type-Options nosniff
  Header always set X-XSS-Protection "1; mode=block"
#  Header always set Access-Control-Allow-Origin: "*"

  # Debian SSO
  SSLCACertificateFile /var/lib/dsa/sso/ca.crt
  SSLCARevocationCheck chain
  SSLCARevocationFile /var/lib/dsa/sso/ca.crl

  WSGIProcessGroup wafertest
  WSGIScriptAlias / /srv/debconf-web/wafertest.debconf.org/dc19/wsgi.py
  WSGIPassAuthorization On

  <Directory /srv/debconf-web/wafertest.debconf.org/dc19>
    <Files wsgi.py>
      Require all granted
    </Files>
  </Directory>

  Alias /static/ /srv/debconf-web/wafertest.debconf.org/dc19/localstatic/
  Alias /favicon.ico /srv/debconf-web/wafertest.debconf.org/dc19/localstatic/img/favicon/favicon.ico
  <Directory /srv/debconf-web/wafertest.debconf.org/dc19/localstatic/>
    Require all granted

    # A little hacky, but it means we won't accidentally catch non-hashed filenames
    <FilesMatch ".*\.[0-9a-f]{12}\.[a-z0-9]{2,5}$">
      ExpiresActive on
      ExpiresDefault "access plus 1 year"
    </FilesMatch>
  </Directory>

  Alias /media/ /srv/debconf-web/wafertest.debconf.org/dc19/media/
  <Directory /srv/debconf-web/wafertest.debconf.org/dc19/media/>
    Require all granted
  </Directory>

  <Location /accounts/debian-login>
    SSLOptions +StdEnvVars
    # Allow access if one does not have a valid certificate
    SSLVerifyClient optional
  </Location>
</VirtualHost>

# vim: set ft=apache:
