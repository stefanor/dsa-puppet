class ntpdate {

	if getfromhash($site::nodeinfo, 'broken-rtc') {
		package { [
			'ntpdate',
			'lockfile-progs'
		]:
			ensure => installed
		}

		$ntpservers = $::hostname ? {
			default => ['manda-node03.debian.org', 'manda-node04.debian.org', 'bm-bl1.debian.org', 'bm-bl2.debian.org']
		}

		file { '/etc/default/ntpdate':
			content => template('ntpdate/etc-default-ntpdate.erb'),
		}
	}
}
