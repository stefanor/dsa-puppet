# == Class: debian_org
#
# Stuff common to all debian.org servers
#
class debian_org::apt {
	if versioncmp($::lsbmajdistrelease, '8') <= 0 {
		$fallbackmirror = 'http://cdn-fastly.deb.debian.org/debian/'
	} else {
		$fallbackmirror = 'http://deb.debian.org/debian/'
	}

	if getfromhash($site::nodeinfo, 'hoster', 'mirror-debian') {
		$mirror = [ getfromhash($site::nodeinfo, 'hoster', 'mirror-debian'), $fallbackmirror ]
	} else {
		$mirror = [ $fallbackmirror ]
	}

	# jessie
	if versioncmp($::lsbmajdistrelease, '8') <= 0 {
		site::aptrepo { 'debian':
			url        => $mirror,
			suite      => [ $::lsbdistcodename ],
			components => ['main','contrib','non-free']
		}
	# stretch or buster
	} else {
		site::aptrepo { 'debian':
			url        => $mirror,
			suite      => [ $::lsbdistcodename, "${::lsbdistcodename}-backports", "${::lsbdistcodename}-updates" ],
			components => ['main','contrib','non-free']
		}
	}

	if versioncmp($::lsbmajdistrelease, '8') <= 0 {
		site::aptrepo { 'security':
			url        => [ 'http://security-cdn.debian.org/', 'http://security.debian.org/' ],
			suite      => "${::lsbdistcodename}/updates",
			components => ['main','contrib','non-free']
		}
	} else {
		site::aptrepo { 'security':
			url        => [ 'http://security.debian.org/' ],
			suite      => "${::lsbdistcodename}/updates",
			components => ['main','contrib','non-free']
		}
	}

	# ca-certificates is installed by the ssl module
	if versioncmp($::lsbmajdistrelease, '9') <= 0 {
		package { 'apt-transport-https':
			ensure => installed,
		}
	} else {
		# transitional package in buster
		package { 'apt-transport-https':
			ensure => purged,
		}
	}
	$dbdosuites = [ 'debian-all', $::lsbdistcodename ]
	site::aptrepo { 'db.debian.org':
		url        => 'https://db.debian.org/debian-admin',
		suite      => $dbdosuites,
		components => 'main',
		key        => 'puppet:///modules/debian_org/db.debian.org.gpg',
	}

	if ($::hostname in []) {
		site::aptrepo { 'proposed-updates':
			url        => $mirror,
			suite      => "${::lsbdistcodename}-proposed-updates",
			components => ['main','contrib','non-free']
		}
	} else {
		site::aptrepo { 'proposed-updates':
			ensure => absent,
		}
	}

	site::aptrepo { 'debian-cdn':
		ensure => absent,
	}
	site::aptrepo { 'debian.org':
		ensure => absent,
	}
	site::aptrepo { 'debian2':
		ensure => absent,
	}
	site::aptrepo { 'backports2.debian.org':
		ensure => absent,
	}
	site::aptrepo { 'backports.debian.org':
		ensure => absent,
	}
	site::aptrepo { 'volatile':
		ensure => absent,
	}
	site::aptrepo { 'db.debian.org-suite':
		ensure => absent,
	}
	site::aptrepo { 'debian-lts':
		ensure => absent,
	}




	file { '/etc/apt/trusted-keys.d':
		ensure => absent,
		force  => true,
	}

	file { '/etc/apt/trusted.gpg':
		mode    => '0600',
		content => "",
	}

	file { '/etc/apt/preferences':
		source => 'puppet:///modules/debian_org/apt.preferences',
	}
	file { '/etc/apt/apt.conf.d/local-compression':
		source => 'puppet:///modules/debian_org/apt.conf.d/local-compression',
	}
	file { '/etc/apt/apt.conf.d/local-recommends':
		source => 'puppet:///modules/debian_org/apt.conf.d/local-recommends',
	}
	file { '/etc/apt/apt.conf.d/local-pdiffs':
		source => 'puppet:///modules/debian_org/apt.conf.d/local-pdiffs',
	}
	file { '/etc/apt/apt.conf.d/local-langs':
		source => 'puppet:///modules/debian_org/apt.conf.d/local-langs',
	}
	file { '/etc/apt/apt.conf.d/local-cainfo':
		source => 'puppet:///modules/debian_org/apt.conf.d/local-cainfo',
	}
	file { '/etc/apt/apt.conf.d/local-pkglist':
		source => 'puppet:///modules/debian_org/apt.conf.d/local-pkglist',
	}

	exec { 'dpkg list':
		command => 'dpkg-query -W -f \'${Package}\n\' > /var/lib/misc/thishost/pkglist',
		creates => '/var/lib/misc/thishost/pkglist',
	}

	exec { 'apt-get update':
		path    => '/usr/bin:/usr/sbin:/bin:/sbin',
		onlyif  => '/usr/local/bin/check_for_updates',
		require => File['/usr/local/bin/check_for_updates']
	}
	Exec['apt-get update']->Package<| tag == extra_repo |>
}
