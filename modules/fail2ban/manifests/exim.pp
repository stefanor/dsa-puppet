class fail2ban::exim inherits fail2ban {
	file { '/etc/fail2ban/filter.d/dsa-exim.conf':
		source => 'puppet:///modules/fail2ban/filter/dsa-exim.conf',
		notify  => Service['fail2ban'],
	}
	file { '/etc/fail2ban/jail.d/dsa-exim.conf':
		source => 'puppet:///modules/fail2ban/jail/dsa-exim.conf',
		notify  => Service['fail2ban'],
	}
}
