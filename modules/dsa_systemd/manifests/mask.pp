define dsa_systemd::mask (
	$ensure = present,
	) {
	$filename = "/etc/systemd/system/${name}"

	case $ensure {
		present: {
			$linkensure = "link"
		}
		absent:  {
			$linkensure = "absent"
		}
		default: { fail ( "Unknown ensure value: '$ensure'" ) }
	}

	file { $filename:
		ensure => $linkensure,
		target => '/dev/null',
		notify => Exec['systemctl daemon-reload'],
	}
}
