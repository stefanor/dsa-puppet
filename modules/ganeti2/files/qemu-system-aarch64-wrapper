#!/bin/bash

##
## THIS FILE IS UNDER PUPPET CONTROL. DON'T EDIT IT HERE.
## USE: git clone git+ssh://$USER@puppet.debian.org/srv/puppet.debian.org/git/dsa-puppet.git
##

##
## Wrapper to call qemu-system-aarch64 from Ganeti, removing all x86/PCI
## assumptions.
##

# Default to virt machine, forcing a gic version 3 to support more than 8 vcpus
machine="virt"

# We want to always enable KVM. Contrary to what the Ganeti's documentation
# says, enabling kvm_flag is not enough.
args+=("-enable-kvm")

while [ "$1" ]; do
    case "$1" in
        -machine|-M)
            # Ignore the machine argument (it will be defined below),
            # except when querying the list of machines
            if [ "$2" = "?" ] ; then
                machine="?"
            fi
            shift
            ;;
        -smp)
            # Check if the CPU is configured to run in 32-bit mode and
            # force the gvic to version 2 in that case
            args+=("$1")
            args+=("$2")
            if test "$2" -gt 8 ; then
                machine="virt,gic-version=3"
            fi
            shift
            ;;
        *)
            # Replace PCI devices by their bus agnostic variant, and drop
            # all the PCI related options
            args+=("$(echo $1 | sed -e 's/virtio-\(\w\+\)-pci/virtio-\1-device/' -e 's/,bus=[^,]\+//' -e 's/,addr=[^,]\+//')")
            ;;
    esac
    shift
done

# With qemu-system-aarch64 the machine should be defined even for simple
# commands like querying the list of devices.
args+=("-machine")
args+=($machine)

# Start qemu-system-aarch64 with the mangled arguments
exec /usr/bin/qemu-system-aarch64 "${args[@]}"
