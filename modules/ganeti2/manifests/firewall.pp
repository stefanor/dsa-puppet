class ganeti2::firewall {

	include ganeti2::params

	$ganeti_hosts = $ganeti2::params::ganeti_hosts
	$ganeti_priv  = $ganeti2::params::ganeti_priv
	$drbd         = $ganeti2::params::drbd

	@ferm::conf { 'ganeti2':
		content => template('ganeti2/defs.conf.erb')
	}

	@ferm::rule { 'dsa-ganeti-noded':
		description => 'allow ganeti-noded communication',
		domain      => '(ip ip6)',
		rule        => 'proto tcp mod state state (NEW) dport (1811) @subchain \'ganeti-noded\' { saddr ($HOST_GANETI) daddr ($HOST_GANETI) ACCEPT; }',
		notarule    => true,
	}

	@ferm::rule { 'dsa-ganeti-confd':
		description => 'allow ganeti-confd communication',
		domain      => '(ip ip6)',
		rule        => 'proto udp mod state state (NEW) dport (1814) @subchain \'ganeti-confd\' { saddr ($HOST_GANETI) daddr ($HOST_GANETI) ACCEPT; }',
		notarule    => true,
	}

	@ferm::rule { 'dsa-ganeti-rapi':
		description => 'allow ganeti-rapi communication',
		domain      => '(ip ip6)',
		rule        => 'proto tcp mod state state (NEW) dport (5080) @subchain \'ganeti-rapi\' { saddr ($HOST_GANETI) daddr ($HOST_GANETI) ACCEPT; }',
		notarule    => true,
	}

	@ferm::rule { 'dsa-ganeti-kvm-migration':
		description => 'allow ganeti kvm migration ',
		domain      => '(ip ip6)',
		rule        => 'proto tcp dport 8102 @subchain \'ganeti-kvm-migration\' { saddr ($HOST_GANETI_BACKEND) daddr ($HOST_GANETI_BACKEND) ACCEPT; }',
		notarule    => true,
	}

	@ferm::rule { 'dsa-ganeti-ssh':
		description => 'allow ganeti to ssh around',
		domain      => '(ip ip6)',
		rule        => 'proto tcp dport ssh @subchain \'ganeti-ssh\' { saddr ( $HOST_GANETI    $HOST_GANETI_BACKEND) ACCEPT; }',
		notarule    => true,
	}

	if $drbd {
		@ferm::rule { 'dsa-ganeti-drbd':
			description => 'allow ganeti drbd communication',
			domain      => '(ip ip6)',
			rule        => 'proto tcp mod state state (NEW) dport (11000:11999) @subchain \'ganeti-drbd\' { saddr ($HOST_GANETI_BACKEND) daddr ($HOST_GANETI_BACKEND) ACCEPT; }',
			notarule    => true,
		}
	}
}
